/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.common.struc;

import java.util.*;


public class BinaryCluster<E>
{
	public BinaryCluster<E> parent, a, b;
	public E data;
	public enum MODE {SINGLE, AVERAGE};
	
	
	public boolean isRoot()
	{
		return parent == null;
	}

	public boolean hasChildren()
	{
		return null != a || null != b;
	}
	
	public void setA(BinaryCluster<E> other)
	{
		if (null!=other.parent)
			throw new RuntimeException("Overwriting other cluster's parent!");
		a = other;
		other.parent = this;
	}
	public void setB(BinaryCluster<E> other)
	{
		if (null!=other.parent)
			throw new RuntimeException("Overwriting other cluster's parent!");
		b = other;
		other.parent = this;
	}
	public void setParent(BinaryCluster<E> other)
	{
		if (null!=this.parent)
			throw new RuntimeException("Overwriting this cluster's parent!");
		if (null == other.a)
			other.a = this;
		else if (null == other.b)
			other.b = this;
		else
			throw new RuntimeException("Overwriting other cluster's child! other.a="+other.a+" other.b="+other.b);
		this.parent = other;
	}
	
	public void getData(Collection<E> dataBuffer)
	{
		if (null!=data)
			dataBuffer.add(data);
		if (a!=null)
			a.getData(dataBuffer);
		if (b!=null)
			b.getData(dataBuffer);
	}
	
	public int countLeaves()
	{
	  if (!hasChildren())
	    return 1;
	  
	  int leaves=0;
	  if (null!=a)
	  {
	    leaves += a.countLeaves();
	  }
	  if (null!=b)
	  {
	    leaves += b.countLeaves();
	  }
	  return leaves;
	}
	
	
	@Override
	public String toString()
	{
		// Assume only nodes without children have data
		return null != data ? data.toString() : "{"+(null==a?"":a)+":"+(null==b?"":b)+"}";
	}
	
	
	public static <E> List<BinaryCluster<E>> doHierarchicalClustering(double[][] similMatrix, E[] dataObjects, double similCutoff, MODE linkageMode)
	{
		if (similMatrix.length != similMatrix[0].length || similMatrix.length != similMatrix[similMatrix.length-1].length)
			throw new IllegalArgumentException("Please provide a square, symmetrical similarity matrix.");
		if (similMatrix.length != dataObjects.length)
			throw new IllegalArgumentException("Matrix size must match the number of data objects");
		
		
		int numClust = dataObjects.length;
		List<BinaryCluster<E>> clusters = new ArrayList<BinaryCluster<E>>(numClust);
		
		
		// Create one cluster for each data object
		//
		for(E obj : dataObjects)
		{
			BinaryCluster<E> clust = new BinaryCluster<E>();
			clust.data = obj;
			clusters.add(clust);
		}
		
		// Keep merging the two most similar clusters, until there is only one left
		//
		while(numClust > 1)
		{
//			System.err.println("Current matrix:");
//			printMatrix(similMatrix, numClust);
			
			double maxSimil=0; // similarity of the two most similar clusters
			int a=1, b=0; // matrix indeces of the most similar clusters
			
			// Find the most similar pair of clusters
			//
			// NOTE: j is always smaller than i. Thus, b is always smaller than a
			//
			for(int i=1; i<numClust; i++)
			{
				for(int j=0; j<i; j++)
				{
					if (similMatrix[i][j] > maxSimil)
					{
						maxSimil = similMatrix[i][j];
						a=i;
						b=j;
					}
				}
			}
			
			if (maxSimil < similCutoff)
			{
				// Stop clustering, when we have clusters left that are less similar than a given cutoff
				break;
			}
			
//			System.out.println("DEBUG: Clustering iteration "+(dataObjects.length-numClust)+" a="+a+" b="+b+" simil="+maxSimil);
			
			// Merge the two clusters
			//
			BinaryCluster<E> newc = new BinaryCluster<E>();
			newc.setA(clusters.get(b));
			newc.setB(clusters.get(a));
			clusters.remove(a);
			clusters.set(b, newc);
			
			// Delete matrix rows & columns for the old clusters
			// Make a new matrix row & column for the merged cluster
			//
			
			switch (linkageMode)
			{
				case SINGLE:
				{
          // Replace matrix row b with the "minimum distance" or "maximum similarity" values from rows a and b
					for(int i=0; i<numClust; i++)
					{
						similMatrix[i][b] = similMatrix[b][i] = Math.max(similMatrix[b][i], similMatrix[a][i]);
					}
					break;
				}
				case AVERAGE:
				{
					// Replace matrix row b with the "minimum distance" or "maximum similarity" values from rows a and b
					int size_a = newc.a.countLeaves();
					int size_b = newc.b.countLeaves();
					for(int i=0; i<numClust; i++)
					{
						similMatrix[i][b] = similMatrix[b][i] = (similMatrix[b][i] * size_b + similMatrix[a][i] * size_a) / (size_a + size_b);
					}
					break;
				}
				default:
				{
					throw new RuntimeException("Unhandled linkageMode "+linkageMode);
				}
			}
			
//			System.err.printf("After merging %s and %s (%f):\n", a, b, maxSimil);
//			printMatrix(similMatrix, numClust);
			
			
			// Delete matrix row a, moving the following matrix rows up
			for(int i=a+1; i<numClust; i++)
			{
				similMatrix[i-1] = similMatrix[i];
			}
			
//			System.err.println("After deleting a row:");
//			printMatrix(similMatrix, numClust);
			
			
			// Delete matrix column a, moving the following matrix columns left, (but we're only doing it for half of the matrix)
			for(int i=0; i<numClust-1; i++)
			{
				for(int j=a+1; j<=numClust-1; j++)
				{
					similMatrix[i][j-1] = similMatrix[i][j];
				}
				similMatrix[i][numClust-1] = -1; // mark deleted column as invalid
			}
			
//			System.err.println("After deleting a column:");
//			printMatrix(similMatrix, numClust);
			
			// Decrement the number of remaining clusters
			numClust--;
			similMatrix[numClust]=null; // not needed, but might avoid some bugs
		}
		
		if (clusters.size()==0)
			throw new RuntimeException("No clusters left... something went wrong");
		
		return clusters;
	}
	
	@SuppressWarnings("unused")
	private static void printMatrix(double[][] similMatrix, int numClust)
	{
		for(int i=0; i<numClust; i++)
		{
			for(int j=0; j<numClust; j++)
			{
//				if (j<i)
					System.err.printf(" %3d.%02d", (int)similMatrix[i][j], (int)(100*(similMatrix[i][j]-(int)similMatrix[i][j])));
//				else
//					System.err.print("    .  ");
			}
			System.err.println();
		}
		System.err.println();
	}
}
