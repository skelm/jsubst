/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.common.struc;

import sk.common.StringFunctions;


public class DoubleMatrix implements Cloneable, NumMatrix
{
	private static final long serialVersionUID = 1L;
	
	double[][] data;
	
	public DoubleMatrix(int xsize)
	{
		data = new double[xsize][xsize];
	}
	public DoubleMatrix(int xsize, int ysize)
	{
		data = new double[xsize][ysize];
	}
	
	public void incrementAll(int value)
	{
		for(int i=0; i<data.length; i++)
			for(int j=0; j<data[0].length; j++)
				data[i][j] += value;
	}
	public void incrementAll(double value)
	{
		for(int i=0; i<data.length; i++)
			for(int j=0; j<data[0].length; j++)
				data[i][j] += value;
	}
	
	
	public void multiplyAll(int factor)
	{
		for(int i=0; i<data.length; i++)
			for(int j=0; j<data[0].length; j++)
				data[i][j] *= factor;
	}
	public void multiplyAll(double factor)
	{
		for(int i=0; i<data.length; i++)
			for(int j=0; j<data[0].length; j++)
				data[i][j] *= factor;
	}
	
	
	
	public void increment(int x, int y, int value)
	{
		data[x][y] += value;
	}
	public void increment(int x, int y, double value)
	{
		data[x][y] += value;
	}
	
	public void set(int x, int y, double value)
	{
		data[x][y]=value;
	}
	public void set(int x, int y, long value)
	{
		data[x][y]=value;
	}
	public void set(int x, int y, int value)
	{
		data[x][y]=value;
	}
	public int getInt(int x, int y)
	{
		return (int)round(data[x][y]);
	}
	public long getLong(int x, int y)
	{
		return round(data[x][y]);
	}
	public double getDouble(int x, int y)
	{
		return data[x][y];
	}
	public int sizex()
	{
		return data.length;
	}
	
	public int sizey()
	{
		return data[0].length;
	}
	
	public double getSum()
	{
		double sum = 0;
		for(int i=0; i<data.length; i++)
			for(int j=0; j<data[0].length; j++)
				sum += data[i][j];
		return sum;
	}
	
	@Override
	public DoubleMatrix clone()
	{
		try
		{
			DoubleMatrix mat = (DoubleMatrix) super.clone();
			mat.data = new double[data.length][];
			for(int i=0; i<data.length; i++)
				mat.data[i] = data[i].clone();
			return mat;
		}
		catch (CloneNotSupportedException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String toString()
	{
		return toFloatString();
	}
	
//	private double getMaxAbs()
//	{
//		double max = 0.0;
//		for(int i=0; i<data.length; i++)
//			for(int j=0; j<data.length; j++)
//				if (max<Math.abs(data[i][j]))
//					max=Math.abs(data[i][j]);
//		return max;
//	}
	
//	protected double getColumnMaxAbs(int column, boolean reverse)
	protected double getColumnMaxAbs(int column)
	{
		double max = 0.0;
//		if (!reverse)
//		{
//			for(int i=0; i<data.length; i++)
//			{
//				if (max<Math.abs(data[i][column]))
//				{
//					max=Math.abs(data[i][column]);
//				}
//			}
//		}
//		else
//		{
			for(int i=0; i<data[0].length; i++)
			{
				if (max<Math.abs(data[column][i]))
				{
					max=Math.abs(data[column][i]);
				}
			}
//		}
		return max;
	}
	
//	public String toFloatString()
//	{
//		final int sizex=sizex(), sizey=sizey();
//		String[] formats = new String[sizey];
//		for(int i=0; i<sizey; i++)
//			formats[i] = StringFunctions.getColumnFormat(getColumnMaxAbs(i, false), 2);
//
//		StringBuilder sb = new StringBuilder();
//		for(int x=0; x<sizex; x++)
//		{
//			for(int y=0; y<sizey-1; y++)
//			{
//				sb.append(String.format(formats[y]+' ', getDouble(x,y)));
//			}
//			sb.append(String.format(formats[sizey-1]+'\n', getDouble(x, sizey-1)));
//		}
//		return sb.toString();
//	}
//	public String toFloatStringReverse()
	public String toFloatString()
	{
		final int sizex=sizex(), sizey=sizey();
		String[] formats = new String[sizex];
		for(int i=0; i<sizex; i++)
			formats[i] = StringFunctions.getColumnFormat(getColumnMaxAbs(i), 2);

		StringBuilder sb = new StringBuilder();
		for(int y=0; y<sizey; y++)
		{
			for(int x=0; x<sizex-1; x++)
			{
				sb.append(String.format(formats[x]+' ', getDouble(x,y)));
			}
			sb.append(String.format(formats[sizex-1]+'\n', getDouble(sizex-1, y)));
		}
		return sb.toString();
	}
	
	
	public String toIntString()
	{
		final int sizex=sizex(), sizey=sizey();
		String[] formats = new String[sizex];
		for(int i=0; i<sizex; i++)
			formats[i] = StringFunctions.getColumnFormat(round(getColumnMaxAbs(i)));

		StringBuilder sb = new StringBuilder();
		for(int y=0; y<sizey; y++)
		{
			for(int x=0; x<sizex-1; x++)
			{
				sb.append(String.format(formats[x]+' ', getLong(x,y)));
			}
			sb.append(String.format(formats[sizex-1]+'\n', getLong(sizex-1, y)));
		}
		return sb.toString();
		
//		final String format = StringFunctions.getColumnFormat(round(getMaxAbs()));
//		
//		StringBuilder sb = new StringBuilder();
//		for(int j=0; j<data[0].length; j++)
//		{
//			for(int i=0; i<data.length-1; i++)
//			{
//				sb.append(String.format(format+" ", round(data[i][j])));
//			}
//			sb.append(String.format(format+"\n", round(data[data.length-1][j])));
//		}
//		return sb.toString();
	}
	

//	public String toIntString()
//	{
//		final int sizex=sizex(), sizey=sizey();
//		String[] formats = new String[sizey];
//		for(int i=0; i<sizey; i++)
//			formats[i] = StringFunctions.getColumnFormat(round(getColumnMaxAbs(i, false)));
//
//		StringBuilder sb = new StringBuilder();
//		for(int x=0; x<sizex; x++)
//		{
//			for(int y=0; y<sizey-1; y++)
//			{
//				sb.append(String.format(formats[y]+' ', round(getDouble(x,y))));
//			}
//			sb.append(String.format(formats[sizey-1]+'\n', round(getDouble(x, sizey-1))));
//		}
//		return sb.toString();
//		
////		final String format = StringFunctions.getColumnFormat(round(getMaxAbs()));
////		
////		StringBuilder sb = new StringBuilder();
////		for(double[] row : data)
////		{
////			for(int i=0; i<row.length-1; i++)
////			{
////				sb.append(String.format(format+" ", round(row[i])));
////			}
////			sb.append(String.format(format+"\n", round(row[row.length-1])));
////		}
////		return sb.toString();
//	}
//	public String toIntStringReverse()
	
	
	/**
	 * Creates an array copy of the values contained in this matrix object.
	 * @return an array copy of the values in this matrix
	 */
	public double[][] toArray()
	{
		double[][] arr = new double[data.length][];
		for(int i=0; i<data.length; i++)
			arr[i] = data[i].clone();
		return arr;
	}
	
	
	protected static final long round(double value)
	{
		if (value > 0.0)
			return (long)Math.ceil(value);
		else
			return (long)Math.floor(value);
	}
	
	public static void main(String[] args)
	{
		// Testing the toString functions
		//
		DoubleMatrix mat = new DoubleMatrix(21, 17);
		System.out.printf("x=%d, y=%d\n", mat.sizex(), mat.sizey());
		System.out.println();
//		double value=0;
		for(int i=0; i<mat.sizex(); i++)
		{
//			value = Math.floor(value);
//			value+=1;
			for(int j=0; j<mat.sizey(); j++)
			{
//				value+=0.01;
//				mat.set(i, j, value);
				mat.set(i, j, (Math.random()>0.5?1:-1)*(Math.pow(Math.random()*100, 8*Math.random())));
			}
		}
		System.out.println(mat.toIntString());
		
		double[][] a = new double[1][2];
		 a[0][0]=1.0;
		 a[0][1]=163.0;
		
		System.out.println(a[0][0]/a[0][1]);
		
		
//		double value = -1539945803338845.50;
//		System.out.println();
//		System.out.println(value);
//		System.out.println(Math.abs(value));
//		System.out.println(Math.log10(Math.abs(value)));
//		System.out.println(1+Math.floor(Math.log10(Math.abs(value))));
//		String format = StringFunctions.getColumnFormat(value, 2);
//		System.out.println(format);
//		System.out.printf(format+"\n",  value);
//		System.out.printf(format+"\n", -value);
//		System.out.printf(format+"\n",  value/15);
//		System.out.printf(format+"\n", -value/15);
//		System.out.printf(format+"\n",  value/150);
//		System.out.printf(format+"\n", -value/150);
//		System.out.printf(format+"\n",  value*10);
//		System.out.printf(format+"\n", -value*10);
	}
}
