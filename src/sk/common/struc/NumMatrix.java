/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.common.struc;

import java.io.Serializable;


public interface NumMatrix extends Serializable, Cloneable
{
	public abstract void increment(int x, int y, int value);
	public abstract void increment(int x, int y, double value);
	
	public abstract void incrementAll(int value);
	public abstract void incrementAll(double value);
	
	public abstract void multiplyAll(int factor);
	public abstract void multiplyAll(double factor);
	
	public abstract void set(int x, int y, double value);
	public abstract void set(int x, int y, long value);
	public abstract void set(int x, int y, int value);
	
	public abstract int getInt(int x, int y);
	public abstract long getLong(int x, int y);
	public abstract double getDouble(int x, int y);
	
	public abstract int sizex();
	
	public abstract int sizey();
	
	public abstract double getSum();
	
	/**
	 * @return a String representation of this matrix, with all values rounded to the closest integer furthest away from 0
	 */
	public abstract String toIntString();
//	/**
//	 * @return a String representation of the reversed matrix, with all values rounded to the closest integer furthest away from 0
//	 */
//	public abstract String toIntStringReverse();
	
	/**
	 * @return a String representation of this matrix, with all values rounded to 2 decimals
	 */
	public abstract String toFloatString();
//	/**
//	 * @return a String representation of the reversed matrix, with all values rounded to 2 decimals
//	 */
//	public abstract String toFloatStringReverse();
	
	public abstract NumMatrix clone();
}
