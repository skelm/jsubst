/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.common;

import java.io.*;
import java.util.zip.*;


public abstract class Serialization
{
	public static void serialize(Serializable object, OutputStream output) throws IOException
	{
		ZipOutputStream zout = new ZipOutputStream(output);
		zout.setLevel(9);
		zout.putNextEntry(new ZipEntry("serialize.out"));
		ObjectOutputStream out = new ObjectOutputStream(zout);
		out.writeObject(object);
		zout.finish();
		out.close();
	}
	public static byte[] serialize(Serializable object) throws IOException
	{
		ByteArrayOutputStream serializedForm = new ByteArrayOutputStream();
		serialize(object, serializedForm);
		byte[] ser = serializedForm.toByteArray();
		serializedForm.close();
		return ser;
	}
	public static void serializeToFile(Serializable object, String fileName) throws IOException
	{
		OutputStream out = new FileOutputStream(fileName);
		serialize(object, out);
		out.close();
	}
	
	
//	public static Object deserialize(String input)
//	{
//		return deserialize(input.getBytes());
//	}
	public static Object deserialize(byte[] input)
	{
		return deserialize(new ByteArrayInputStream(input));
	}
	public static Object deserialize(InputStream input)
	{
		try
		{
			ZipInputStream zin = new ZipInputStream(input);
			if (zin.getNextEntry() == null)
				return null;
			ObjectInputStream in = new ObjectInputStream(zin);
			Object obj = in.readObject();	
			in.close();
			return obj;
		}
		catch (IOException e)
		{
			Exceptions.handleException(e);
		}
		catch (ClassNotFoundException e)
		{
			Exceptions.handleException(e);
		}
		return null;
	}
	public static Object deserializeFromFile(String fileName)
	{
		try
		{
			InputStream in = new FileInputStream(fileName);
			Object o = deserialize(in);
			in.close();
			return o;
		}
		catch (IOException e)
		{
			Exceptions.handleException(e);
		}
		return null;
	}
}
