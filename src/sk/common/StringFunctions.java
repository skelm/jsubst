/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.common;

import java.util.*;
import java.util.regex.Pattern;

public abstract class StringFunctions
{
	protected static final Pattern BLANK = Pattern.compile("\\s+");
	
	public static String getColumnFormat(double value, int decimals)
	{
		return "% "+getColumnWidth(value, decimals)+"."+decimals+"f";
	}
	public static String getColumnFormat(long value)
	{
		return "% "+getColumnWidth(value)+"d";
	}
	public static int getColumnWidth(long value)
	{
		value = Math.max(Math.abs(value), 1L);
		return 2+(int)Math.floor(Math.log10(value));
	}
	public static int getColumnWidth(double value, int decimals)
	{
		value = Math.max(Math.abs(value), 1.0);
		return decimals+3+(int)Math.floor(Math.log10(value));
	}
	
	public static String join(String glue, Object[] arr)
	{
		if (arr.length < 1)
			return "";
		
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<arr.length; i++)
		{
			if (arr[i] == null)
				continue;
			sb.append(glue);
			sb.append(arr[i]);
		}
		if (sb.length() > 0)
			sb.delete(0, glue.length());
		return sb.toString();
	}
	
	public static String join(String glue, Collection<?> coll)
	{
		if (coll.size() < 1)
			return "";
		
		StringBuffer sb = new StringBuffer();
		Iterator<?> i = coll.iterator();
		while(i.hasNext())
		{
			Object o = i.next();
			if (o == null)
				continue;
			sb.append(glue);
			sb.append(o);
		}
		if (sb.length() > 0)
			sb.delete(0, glue.length());
		return sb.toString();
	}
}
