/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

public class Sequence implements Comparable<Sequence>
{
	private String name;
	private String type;
	private String sequence;
//	private int[] intSequence;

	public Sequence(String name, String type)
	{
		this.name = name;
		this.type = type;
	}
	public Sequence(String name, String type, String sequence)
	{
		this.name = name;
		this.type = type;
		setSequence(sequence);
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getType()
	{
		return type;
	}

	public String getSequence()
	{
		return sequence;
	}
	public char getSequenceCharAt(int index)
	{
		return sequence.charAt(index);
	}
//	public int getSequenceIntAt(int index)
//	{
//		return intSequence[index];
//	}

	public int compareTo(Sequence other)
	{
		int cmp = name.compareTo(other.name);
		if (0 == cmp)
		{
			cmp = type.compareTo(other.type);
			if (0 == cmp)
				cmp = sequence.compareTo(other.sequence);
		}
		return cmp;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Sequence))
			return false;
		return 0 == compareTo((Sequence)other);
	}
	
	@Override
	public int hashCode()
	{
		return name.hashCode() + type.hashCode() + sequence.hashCode();
	}
	
	protected void setSequence(String sequence)
	{
		this.sequence = sequence;
		
//		int len=sequence.length();
//		this.intSequence = new int[len];
//		for(int i=0; i<len; i++)
//		{
//			intSequence[i]=Env.getAAIndex(sequence.charAt(i));
//		}
	}
	
	@Override
	public String toString()
	{
		return "["+name+":"+type+":"+sequence+"]";
	}
}
