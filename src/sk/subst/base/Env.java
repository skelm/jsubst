/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;


import java.io.*;
import java.util.*;

import sk.common.struc.*;



public class Env implements Serializable, Comparable<Env>
{
	private static final long serialVersionUID = 1L;
	
	
	private static char[] aminoAcids = "GAVLMIFYWSTCPNQKRHDE".toCharArray();
	//private static char[] aminoAcids = "ACDEFGHIKLMNPQRSTVWY".toCharArray();
	
	
	private static final double logFactor = 3 / Math.log(2);
	
	
	private char[] value;
	private String label;
	
	private NumMatrix counts = new SubstMatrix(aminoAcids);
	private CompositionList compositionCounts = new CompositionList(aminoAcids);
	private NumMatrix probab;
	private NumMatrix logodd;
	
	public Env(String label)
	{
		this.label = label;
	}
	
	public Env(char[] value, String label)
	{
		if (value.length != label.length())
			throw new IllegalArgumentException(
					"Value and label of different length");
		this.value = value;
		this.label = label;
	}
	
	/***
	 * @return the length of this Env's String representation
	 */
	public int length()
	{
		return value.length;
	}
	
	/***************************************************************************
	 * @return this Env object's string representation (e.g. "HASoN")
	 */
	public String getLabel()
	{
		return label;
	}
	
	/***************************************************************************
	 * Check if the given sequence of environment characters (e.g. H = alpha
	 * helix, E = beta sheet, T = true, F = false, ...) corresponds to this Env
	 * object. The given char array is expected to have a certain length and
	 * each position corresponds to a certain environment sub-class.
	 * 
	 * @param value
	 *            a sequence of environment characters
	 * @return true if the given char array is equal to this Env's internal char
	 *         array.
	 */
	public boolean hasValue(char[] value)
	{
		return Arrays.equals(this.value, value);
	}
	
	// protected void setCountMatrix(NumMatrix counts)
	// {
	// this.counts = counts;
	// }
	
	// public void incrementCount(int aa1, int aa2)
	// {
	// counts.increment(aa1, aa2, 1.0);
	// }
	
	public void incrementCount(int aa1, int aa2, double weight)
	{
		counts.increment(aa1, aa2, weight);
	}
	
	public void incrementCompositionCount(int aa1, double weight)
	{
		compositionCounts.increment(aa1, weight);
	}
	
	
	// public void incrementCount(char aa1, char aa2)
	// {
	// if (aa1==aa2)
	// {
	// int index = getAAIndex(aa1);
	// counts.increment(index, index, 1.0);
	// }
	// else
	// {
	// counts.increment(getAAIndex(aa1), getAAIndex(aa2), 1.0);
	// }
	// }
	
	public NumMatrix getCounts()
	{
		return counts;
	}
	
	public NumMatrix getProbab()
	{
		if (null == probab)
			throw new NullPointerException(
					"Call calculateProbab() before calling this method!");
		return probab;
	}
	
	public NumMatrix getLogOdd()
	{
		if (null == logodd)
			throw new NullPointerException(
					"Call calculateLogOdd(double[]) before calling this method!");
		return logodd;
	}
	
	public void calculateProbab()
	{
		// Let A and B be amino acids.
		// Let AtoB be the number of substitutions from amino acid A to amino
		// acid B in the structural environment represented by this object.
		// Let pAtoB be the probability of A being substituted by B in this
		// environment.
		//
		// pAtoB = AtoB / sumOverAllC(AtoC)
		//
		probab = new SubstMatrix(aminoAcids);
		
		int size = counts.sizex();
		for (int a = 0; a < size; a++)
		{
			double sumAtoC = 0;
			for (int c = 0; c < size; c++)
				sumAtoC += counts.getDouble(a, c);
			
			if (0.0 < sumAtoC)
			{
				for (int b = 0; b < size; b++)
				{
					double AtoB = counts.getDouble(a, b);
					probab.set(a, b, AtoB / sumAtoC);
				}
			}
			else
			{
				// System.err.println("sumAtoC is 0 for a="+a);
			}
		}
	}
	
	
	public void calculateLogOdd(double[] bgProbab)
	{
		if (null == probab)
			calculateProbab();
		
		// Let A and B be amino acids.
		// Let AtoB be the number of substitutions from amino acid A to amino
		// acid B in the structural environment represented by this object.
		// Let pAtoB be the probability of A being substituted by B in this
		// environment.
		// Let qB be the background probability of observing amino acid B.
		//
		// logAtoB = log (pAtoB / qB)
		//
		logodd = new SubstMatrix(aminoAcids);
		
		int size = counts.sizex();
		for (int a = 0; a < size; a++)
		{
			for (int b = 0; b < size; b++)
			{
				double pAtoB = probab.getDouble(a, b);
				double qB = bgProbab[b];
				if (0.0 == qB || 0.0 == pAtoB)
				{
					// System.err.println("LOG-ODDS: ("+a+","+b+") qB="+qB+"
					// pAtoB="+pAtoB);
					logodd.set(a, b, 0.0);
				}
				
				else
					logodd.set(a, b, Math.round(logFactor
							* Math.log(pAtoB / qB)));
			}
		}
	}
	
	
	/***************************************************************************
	 * Converts a given amino acid character to its corresponding substitution
	 * matrix index.
	 * 
	 * @return a substitution matrix index
	 */
	public static int toAAIndex(char aa)
	{
		for (int i = 0; i < aminoAcids.length; i++)
			if (aa == aminoAcids[i])
				return i;
		// throw new IllegalArgumentException("Illegal amino acid: '"+aa+"'");
		return -1;
	}
	
	/***************************************************************************
	 * Converts a given String of amino acid characters to its corresponding
	 * sequence of substitution matrix indeces.
	 * 
	 * @return an array of substitution matrix indeces
	 */
	public static int[] toAAIndex(String sequence)
	{
		int len = sequence.length();
		int[] intSequence = new int[len];
		for (int i = 0; i < len; i++)
		{
			intSequence[i] = Env.toAAIndex(sequence.charAt(i));
		}
		return intSequence;
	}
	
	/***************************************************************************
	 * @return a copy of the internal index of amino acid types (such as
	 *         A,C,D,E,...)
	 */
	public static char[] getAAs()
	{
		return aminoAcids.clone();
	}
	
	/***************************************************************************
	 * @return the total number of valid amino acid types (such as A,C,D,E,...)
	 */
	public static int getAAcount()
	{
		return aminoAcids.length;
	}
	
//	/***************************************************************************
//	 * Print information about zero fields in a given amino acid substitution
//	 * matrix.
//	 * 
//	 * @param aaMatrix
//	 *            the matrix
//	 */
//	public static void printZeroInfo(NumMatrix aaMatrix)
//	{
//		int size = aaMatrix.sizex();
//		if (aaMatrix.sizey() != size)
//			throw new IllegalArgumentException("Matrix must be square!");
//		if (getAAcount() != size)
//			throw new IllegalArgumentException("Matrix must be square!");
//		
//		for (int i = 0; i < size; i++)
//		{
//			char aa1 = aminoAcids[i];
//			for (int j = 0; j < size; j++)
//			{
//				if (0.0 == aaMatrix.getDouble(i, j))
//				{
//					char aa2 = aminoAcids[j];
//					System.out.println("# Zero score [" + aa1 + ">" + aa2
//							+ "] (" + i + "," + j + ")");
//				}
//			}
//		}
//	}
//	
//	/***************************************************************************
//	 * Print information about zero fields in a given amino acid substitution
//	 * matrix.
//	 * 
//	 * @param aaMatrix
//	 *            the matrix
//	 * @param exclude
//	 *            the amino acids for which information should NOT be shown
//	 */
//	public static void printZeroInfo(NumMatrix aaMatrix, char... exclude)
//	{
//		int size = aaMatrix.sizex();
//		if (aaMatrix.sizey() != size)
//			throw new IllegalArgumentException("Matrix must be square!");
//		if (getAAcount() != size)
//			throw new IllegalArgumentException("Matrix must be square!");
//		
//		for (int i = 0; i < size; i++)
//		{
//			char aa1 = aminoAcids[i];
//			if (isInArray(aa1, exclude))
//				continue;
//			
//			for (int j = 0; j < size; j++)
//			{
//				if (0.0 == aaMatrix.getDouble(i, j))
//				{
//					char aa2 = aminoAcids[j];
//					if (!isInArray(aa2, exclude))
//						System.out.println("# Zero score [" + aa1 + "->" + aa2
//								+ "] (" + i + "," + j + ")");
//				}
//			}
//		}
//	}
	
	/**
	 * Prints a matrix in human readable format
	 */
	public static void printMatrix(NumMatrix mat, boolean toInt, String title)
	{
		printMatrix(System.out, mat, toInt, title);
	}
	
	/**
	 * Prints a matrix to the specified PrintStream in human readable format
	 * @param out
	 * @param mat
	 * @param toInt
	 * @param title
	 */
	public static void printMatrix(PrintStream out, NumMatrix mat, boolean toInt, String title)
	{
//		out.print('#');
//		for(int i=0; i<title.length(); i++)
//			out.print('=');
//		out.println();
		
//		out.println("!"+title);
		out.println(">"+title);
		
//		out.print('#');
//		for(int i=0; i<title.length(); i++)
//			out.print('=');
//		out.println();
		
//		out.println();
		
		if (toInt)
			out.println(mat.toIntString());
		else
			out.println(mat.toFloatString());
	}
	
	public static void printFlatMatrixLabels(PrintStream out)
	{
		printFlatMatrixLabels(out, "\t");
	}
	public static void printFlatMatrixLabels(PrintStream out, String delim)
	{
		if (null==delim)
			delim="";
		
		StringBuilder sb = new StringBuilder();
		// from c1 (x)
		for (char c1 : aminoAcids)
		{
			// to c2 (y)
			for (char c2 : aminoAcids)
			{
				sb.append(""+c1+">"+c2+delim);
			}
		}
		sb.delete(sb.length()-delim.length(), sb.length());
		out.println(sb);
	}
	
	/**
	 * Prints a matrix to the specified PrintStream on a single lign
	 * @param out
	 * @param mat
	 * @param toInt
	 * @param title
	 */
	public static void printFlatMatrix(PrintStream out, NumMatrix mat, boolean toInt, String title)
	{
		printFlatMatrix(out, mat, toInt, title, "\t");
	}
	/**
	 * Prints a matrix on a single line
	 * @param out
	 * @param mat
	 * @param toInt
	 * @param title
	 * @param delim
	 */
	public static void printFlatMatrix(PrintStream out, NumMatrix mat, boolean toInt, String title, String delim)
	{
		out.print(title);
		out.print(delim);
		
		int sizex = mat.sizex();
		int sizey = mat.sizey();
		
		// x = from
		for(int x=0; x<sizex; x++)
		{
			// y = to
			for(int y=0; y<sizey; y++)
			{
				if (toInt)
					out.print(mat.getLong(x,y));
				else
					out.print(mat.getDouble(x,y));
				if (x<sizex-1 || y<sizey-1)
					out.print(delim);
			}
		}
		
		out.println();
	}
	
	public static String getValidAminoAcids()
	{
		return String.copyValueOf(aminoAcids);
	}
	public static void setValidAminoAcids(String validAcids)
	{
		aminoAcids = validAcids.toCharArray();
	}
	
	
//	/***************************************************************************
//	 * Adds amino acid labels to the upper and left edges of an existing String
//	 * representation of a substitution matrix.
//	 * 
//	 * @param matrix
//	 *            the String representation of a substitution matrix
//	 * @return the new labeled String representation of a substitution matrix
//	 */
//	public static String annotateMatrixString(String matrix)
//	{
//		String[] lines = matrix.split("\n", aminoAcids.length);
//		StringBuilder result = new StringBuilder(matrix.length() + 2
//				* aminoAcids.length + lines[0].length() + 3);
//		int colWidth = (lines[0].length() + 1) / aminoAcids.length - 1;
//		
//		// Add column headers
//		//
//		result.append("  ");
//		String format = "%" + colWidth + "s ";
//		for (int i = 0; i < aminoAcids.length - 1; i++)
//		{
//			try
//			{
//				result.append(String.format(format, aminoAcids[i]));
//			}
//			catch (FormatFlagsConversionMismatchException e)
//			{
//				// System.err.println("String.format() error in
//				// Env.annotateMatrixString(): "+e.getMessage());
//				// System.err.println("i="+i+" colWidth="+colWidth+"
//				// char="+aminoAcids[i]);
//				appendChar(result, aminoAcids[i], colWidth);
//				result.append(' ');
//			}
//		}
//		appendChar(result, aminoAcids[aminoAcids.length - 1], colWidth);
//		result.append("\n\n");
//		
//		// Add row headers
//		//
//		for (int i = 0; i < lines.length; i++)
//		{
//			result.append(aminoAcids[i]);
//			result.append(' ');
//			result.append(lines[i]);
//			result.append('\n');
//		}
//		return result.toString();
//	}
	
//	private static final void appendChar(StringBuilder sb, char val,
//			int colWidth)
//	{
//		for (int j = 0; j < colWidth - 1; j++)
//			sb.append(' ');
//		sb.append(val);
//	}
	
//	private static final boolean isInArray(char val, char[] arr)
//	{
//		for (char c : arr)
//			if (c == val)
//				return true;
//		return false;
//		
//	}
	
	/***************************************************************************
	 * @param source
	 *            the Env object to be copied
	 * @return an equal Env object, but without any amino acid counts,
	 *         probabilities or log-odds scores.
	 */
	public static Env copyWithoutCounts(Env source)
	{
		return new Env(source.value, source.label);
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Env))
			return false;
		return this.label.equals(((Env)other).label);
	}
	
	@Override
	public int hashCode()
	{
		return label.hashCode();
	}
	
	@Override
	public String toString()
	{
		return "{" + label + ":" + Arrays.toString(value) + ":"
				+ (null == counts ? 0 : counts.getSum()) + ")";
	}
	
	public int compareTo(Env other)
	{
		return this.label.compareTo(other.label);
	}

	public CompositionList getCompositionCounts() {
		return compositionCounts;
	}

	public void setCompositionCounts(CompositionList compositionCounts) {
		this.compositionCounts = compositionCounts;
	}
}
