/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

import java.util.ArrayList;

import sk.common.StringFunctions;

public class CompositionList extends ArrayList<Double>
{
	private static final long serialVersionUID = 1L;
	
	private char[] labels;
	
	public CompositionList(char[] labels)
	{
		super(labels.length);
		for (int i=0; i<labels.length; ++i)
		{
			add(0.0);
		}
		this.labels = labels;
	}
	
	public String getHeaderString()
	{
		final int size=labels.length;
		final int decimals=2;
		int[] colwidth = new int[size];
		for(int i=0; i<size; i++)
			//colwidth[i] = StringFunctions.getColumnWidth(Math.abs(get(i)), 2);
			colwidth[i] = 10;
		
		StringBuilder sb = new StringBuilder();
		
		// Column headers
		
		for(int i=0; i<size-1; i++)
		{
			sb.append(String.format("%"+colwidth[i]+"s ", labels[i]));
		}
		sb.append(String.format("%"+colwidth[size-1]+"s\n", labels[size-1]));
		return sb.toString();
	}
	
	public String toFloatString()
	{
		final int size=labels.length;
		final int decimals=2;
		int[] colwidth = new int[size];
		for(int i=0; i<size; i++)
			//colwidth[i] = StringFunctions.getColumnWidth(Math.abs(get(i)), 2);
			colwidth[i] = 10;
		
		StringBuilder sb = new StringBuilder();
		
		// Column headers
		
		for(int x=0; x<size-1; x++)
		{
			sb.append(String.format("% "+colwidth[x]+"."+decimals+"f ", get(x)));
		}
		sb.append(String.format("% "+colwidth[size-1]+"."+decimals+"f\n", get(size-1)));
		return sb.toString();
	}
	
	public void increment(int i, double weight)
	{
		set(i, get(i)+weight);
	}
		
	public static void main(String[] args)
	{
		// Testing the toString functions
		//
		CompositionList lst = new CompositionList(Env.getAAs());
		double value=0;
		for(int i=0; i<lst.size(); i++)
		{
			value+=1;
			lst.set(i, value);
		}
		System.out.println(lst.toFloatString());
		
		//DoubleMatrix mat2 = mat.clone();
		
		//mat2.multiplyAll(2);
		//System.out.println("==============================");
		//System.out.println(mat2.toIntString());
		System.out.println("==============================");
		System.out.println(lst.toFloatString());
		System.out.println("==============================");
	}
}
