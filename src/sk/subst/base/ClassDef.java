/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

import java.io.*;
import java.util.*;
import java.util.regex.*;



public class ClassDef
{	
	private static final Pattern CLASSDEF_FIELDS = Pattern.compile("^([^;]+);([^;]+);([^;]+);([^;]+);([^;]+)");
	
	public Env[] envirs;
	public List<EnvParam> envParams = new ArrayList<EnvParam>();
	
	public ClassDef(String filename)
	{
		try
		{
			// Parse the classdef file (specified by the first non-option argument),
			// which defines the structural environments.
			//
			parseClassdef(filename);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public EnvParam getEnvParam(String name)
	{
		for (EnvParam ep : envParams)
			if (ep.name.equals(name))
				return ep;
		return null;
	}
	
	
	public void parseClassdef(String filename) throws IOException
	{
		// Input file
		//
		BufferedReader in = new BufferedReader(new FileReader(filename));
		List<EnvParam> classDefs = new ArrayList<EnvParam>();
		
		LinkedList<StringBuilder> classValues = new LinkedList<StringBuilder>();
		LinkedList<StringBuilder> classLabels = new LinkedList<StringBuilder>();
		classValues.add(new StringBuilder());
		classLabels.add(new StringBuilder());
		
		String line;
		while(null != (line = in.readLine()))
		{
			if (line.startsWith("#") || line.startsWith("!"))
				continue;
			
			Matcher m = CLASSDEF_FIELDS.matcher(line);
			if (!m.find())
				throw new IOException("Classdef line does not match standard pattern:\n\t"+line);
			EnvParam cd = new EnvParam();
			cd.name   = m.group(1).trim();
			cd.values = m.group(2).trim();
			cd.labels = m.group(3).trim();
			cd.constrained = m.group(4).trim().equals("T");
			cd.mask = m.group(5).trim().equals("T");
			
			int len = cd.values.length();
			if (cd.labels.length() != len || 0==len)
				throw new IOException("Illegal Classdef format: Values must have the same (positive) length as labels");
			
			classDefs.add(cd);
		}
		in.close();
		
		// Sort Classdefs by name.
		// This corresponds to the sorting of Sequence objects by type.
		// This is important!!! The ordering corresponds to the ordering
		// of Sequence objects by type. 
		//
		Collections.sort(classDefs);
		
		// Build all possible combinations of values that define structural environments 
		//
		for(EnvParam cd : classDefs)
		{
			if (cd.mask)
			{
				System.err.println("#NOTICE: Found mask: "+cd.name+":"+cd.values+":"+cd.labels+":"+(cd.constrained?"T":"F")+":"+(cd.mask?"T":"F"));
				continue;
			}
			
			System.err.println("#NOTICE: Found environment parameter: "+cd.name+":"+cd.values+":"+cd.labels+":"+(cd.constrained?"T":"F")+":"+(cd.mask?"T":"F"));
			
			ListIterator<StringBuilder> valIter = classValues.listIterator();
			ListIterator<StringBuilder> labIter = classLabels.listIterator();
			while(valIter.hasNext())
			{
				StringBuilder currentVal = valIter.next();
				StringBuilder currentLab = labIter.next();
				int len = cd.values.length();
				
				for (int i=len-1; 0<=i; i--)
				{
					char val = cd.values.charAt(i);
					char lab = cd.labels.charAt(i);
					if (0==i)
					{
						// This is the last iteration of the inner loop.
						// Update the current label and value
						// (no need to create a new one, since we still have the old one),
						// before moving on.
						currentVal.append(val);
						currentLab.append(lab);
					}
					else
					{
						StringBuilder tmp;
						
						// Create new class values
						tmp= new StringBuilder(currentVal);
						tmp.append(val);
						valIter.add(tmp);
						
						// Create new class labels
						tmp = new StringBuilder(currentLab);
						tmp.append(lab);
						labIter.add(tmp);
					}
				}
			}
		}
		
		if (classValues.size()==1 && classValues.get(0).length()==0)
			throw new IOException("Classdef is empty - no non-mask environemnt parameters specified!");
		
//		value2env = new HashMap<String, Env>(classValues.size());
//		Iterator<StringBuilder> valIter = classValues.iterator();
//		Iterator<StringBuilder> labIter = classLabels.iterator();
//		while(valIter.hasNext())
//		{
//			StringBuilder currentVal = valIter.next();
//			StringBuilder currentLab = labIter.next();
//			Env env = new Env(currentVal.toString(), currentLab.toString());
//			value2env.put(env.getValue(), env);
//		}
		
		Env[] environments = new Env[classValues.size()];
		Iterator<StringBuilder> valIter = classValues.iterator();
		Iterator<StringBuilder> labIter = classLabels.iterator();
		int i=0;
		while(valIter.hasNext())
		{
			StringBuilder currentVal = valIter.next();
			StringBuilder currentLab = labIter.next();
			char[] value = new char[currentVal.length()];
			currentVal.getChars(0, value.length, value, 0);
			environments[i++] = new Env(value, currentLab.toString());
		}
		
		Arrays.sort(environments);
		
//		System.out.println("#NOTICE: Number of environments: "+value2env.size());
		System.err.println("#NOTICE: Number of environments: "+environments.length);
		
		this.envParams = classDefs;
		this.envirs = environments;
	}
	
//	public Env getEnv(String value)
//	{
//		return value2env.get(value);
//	}
//	
//	@Override
//	public String toString()
//	{
//		return value2env.toString();
//	}
}
