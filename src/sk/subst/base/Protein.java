/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

import java.util.*;



public class Protein extends Sequence
{
	private List<Sequence> strucInfo;
	private int[] intSequence;
	private boolean[] mask;
	private int number=-1;
	
	public Protein(String name)
	{
		super(name, "sequence");
	}
	
	public Protein(String name, String sequence)
	{
		super(name, "sequence", sequence);
	}
	
	public Protein(String name, String sequence, List<Sequence> strucInfo)
	{
		super(name, "sequence", sequence);
		this.strucInfo = strucInfo;
	}

	public void setNumber(int number)
	{
		this.number = number;
	}

	public int getNumber()
	{
		return number;
	}

	public boolean hasStrucInfo()
	{
		return null != strucInfo;
	}
	
	protected void addStrucInfo(Sequence seq)
	{
		if (null==strucInfo)
			strucInfo = new ArrayList<Sequence>();
		strucInfo.add(seq);
	}
	
	public int getStrucInfoSize()
	{
		return null == strucInfo ? 0 : strucInfo.size();
	}
	
	public Iterator<Sequence> getStrucInfoIterator()
	{
		if (null==strucInfo)
			return null;
		return strucInfo.iterator();
	}
	
	public Sequence getStrucInfo(String type)
	{
		if (null!=strucInfo)
			for (Sequence seq : strucInfo)
				if (seq.getType().equals(type))
					return seq;
		return null;
	}
	
	protected void sortStrucInfo()
	{
		if (null!=strucInfo)
			Collections.sort(strucInfo);
	}
	
	public boolean getEnvCharsAt(int aaPos, char[] buffer)
	{
		int size = strucInfo.size();
		if (size != buffer.length)
		{
			System.err.println(strucInfo);
			throw new IllegalArgumentException("Buffer size must match number of structural information sequences: aaPos="+aaPos+" buffer.length="+buffer.length+" strucInfo.length="+strucInfo.size());
		}
		
		for(int i=0; i<size; i++)
		{
			buffer[i] = strucInfo.get(i).getSequenceCharAt(aaPos);
			if ('-'==buffer[i])
				return false;
		}
		return true;
	}
	
	public int getSequenceIntAt(int aaPos)
	{
		return intSequence[aaPos];
	}
	
	protected void addMask(String sequence)
	{
		if (null == mask)
		{
			mask = new boolean[sequence.length()];
		}
		for (int i=0; i<mask.length; i++)
		{
			mask[i] |= (sequence.charAt(i) == 'T');
		}
	}
	
	public boolean isMaskedAt(int i)
	{
		if (null==mask)
			return false;
		return mask[i];
	}
	
	
	@Override
	protected void setSequence(String sequence)
	{
		int len = sequence.length();
		super.setSequence(sequence);
		this.intSequence = new int[len];
		for(int i=0; i<len; i++)
		{
			intSequence[i]=Env.toAAIndex(sequence.charAt(i));
		}
	}
	
	
	public double getPID(Protein other)
	{
		if (this==other)
			return 100.0;
		if (intSequence.length != other.intSequence.length)
			throw new IllegalArgumentException("Cannot calculate PID for unaligned proteins, whose sequences have different lengths");
		int id=0;
		int n=0;
		
		for(int i=0; i<intSequence.length; i++)
		{
			if ((0 <= intSequence[i]) && (0 <= other.intSequence[i]))
			{
//				System.out.printf("counting %d <-> %d ? %s\n", intSequence[i], other.intSequence[i], String.valueOf(intSequence[i]==other.intSequence[i]));
				n++;
				if (intSequence[i]==other.intSequence[i])
					id++;
			}
//			else
//			{
//				System.out.printf("skipping %d <-> %d\n", intSequence[i], other.intSequence[i]);
//			}
		}
		
		//int n = intSequence.length; // alignment length
		
		if (0==n)
			return 0.0;
		return (100.0*id)/n;
	}
	
	
	@Override
	public int hashCode()
	{
		return super.hashCode() + (hasStrucInfo() ? 1 : 0);
	}
	
	@Override
	public String toString()
	{
//		return "["+getName()+":"+getType()+":"+hasStrucInfo()+":"+getSequence()+"]";
		return "{"+getName().substring(0, Math.min(10, getName().length()))+":"+getType()+":"+hasStrucInfo()+"}";
	}
	
	public static void main(String[] args)
	{
		Protein a, b, c;
		a = new Protein("A", "AAA-GGGCCC");
		b = new Protein("B", "GGG-GG-CCC");
		c = new Protein("C", "CCC--G-CCC");
		
		// These numbers are based on the old PID measure, where we divide by the number of aligned residues.
		// The new measure is divided by the alignment length instead, so the numbers will differ.
		
		System.out.println("A-B "+a.getPID(b)); //  5/8 = .625
		System.out.println("B-A "+b.getPID(a)); //  5/8 = .625
		System.out.println("A-C "+a.getPID(c)); //  4/7 = .571...
		System.out.println("C-A "+c.getPID(a)); //  4/7 = .571...
		System.out.println("B-C "+b.getPID(c)); //  4/7 = .571...
		System.out.println("C-B "+c.getPID(b)); //  4/7 = .571...
		
		Protein p3, p4, p5;
		p3 = new Protein("p3", "IRCFIT--P-DGTSKDCPNG-HVCYTKTWCDGFCSIRGKRVDLGCAATCPTVKTGV-DIQCCST-DNCNPFPTGGAP-");
		p4 = new Protein("p4", "ITCYKT--P-IITSETCAPGQNLCYTKTWCDAWCGSRGKVIELGCAATCPTVESYQ-DIKCCST-DNCNPHPKQKR-P");
		p5 = new Protein("p5", "IVCHTTATI-PSSAVTCPPGENLCYRKMWCDAFCSSRGKVVELGCAATCPSKKPYE-EVTCCST-DKCNHPPKRQ-PG");

		System.out.println("p3-p4 "+p3.getPID(p4));
		System.out.println("p3-p5 "+p3.getPID(p5));
		System.out.println("p4-p5 "+p4.getPID(p5));
	}
}
