/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

import java.io.*;
import java.util.*;
import java.util.regex.*;



public abstract class Tem
{
	private static final Pattern
		TITLE = Pattern.compile("^>(?:P\\d+;)?(.+)$"),
		WHITE_SPACE = Pattern.compile("\\s+"),
		ONLY_WHITE_SPACE = Pattern.compile("^\\s*$");
	
	public static Protein[] parseTemFile(String filename) throws IOException
	{
		return parseTemFile(filename, null);
	}
	public static Protein[] parseTemFile(String filename, ClassDef classdef) throws IOException
	{
//		 Input file
		//
		BufferedReader in = new BufferedReader(new FileReader(filename));
		
		// Archive for complete TEM file entries, grouped by protein
		//
		// We can use identity rather than equality because we are intern()-alising name strings, which are used for keys in this map.
		Map<String, Protein> proteins = new LinkedHashMap<String, Protein>();
		
		// Temporary storage for TEM file entries
		//
		String name=null;
		String type=null;
		StringBuilder seq=null;
		boolean skip=false;
		
		String line; // the current line in the TEM file being parsed
		while (true)
		{
			line = in.readLine();
			// At the end of the file, or at the beginning of a new entry...
			//
			if (null==line || line.startsWith(">"))
			{
				if (null!=seq)
				{
					if (skip)
					{
//						System.err.println("DEBUG: Not using TEM entry of type '"+type+"'");
					}
					else
					{
						// Archive entry in temporary storage to protein hashmap
						//
						
						while ('*' == seq.charAt(seq.length()-1))
							seq.deleteCharAt(seq.length()-1);
						
						String finalSeq;
						Matcher m = WHITE_SPACE.matcher(seq);
						if (m.find())
							finalSeq = m.replaceAll("");
						else
							finalSeq = seq.toString();
						
						Protein prot = proteins.get(name);
						if ("sequence".equals(type) || type.startsWith("structure"))
						{
							if (null==prot)
							{
								prot = new Protein(name, finalSeq);
								proteins.put(name, prot);
							}
							else
							{
								prot.setSequence(finalSeq);
							}
						}
						else
						{
							if (null==prot)
							{
								prot = new Protein(name);
								proteins.put(name, prot);
							}
							
							EnvParam ep = classdef.getEnvParam(type);
							if (null != ep)
							{
								if (ep.mask)
								{
									prot.addMask(finalSeq);
								}
								else
								{
									prot.addStrucInfo(new Sequence(name, type, finalSeq));
								}
//								System.err.println("DEBUG: Saving TEM entry of type '"+type+"'");
							}
							else
							{
								System.err.println("DEBUG: Entry type not found in EnvParams: '"+type+"'");
							}
						}
					}
				}
				if (null==line)
					break;
				
				
				// At the beginning of a new entry...
				//
				
				// Clear previous entry from temporary storage
				//
				skip = false;
				name = null;
				type = null;
				seq  = new StringBuilder();
				
				// Save new entry's name in temporary storage
				//
				Matcher m = TITLE.matcher(line);
				if (m.matches())
					name=m.group(1).intern();
				else
					throw new IOException("Unexpected file format. Descriptor line (beginning with '>') seems to be empty");
			}
			else if (skip || ONLY_WHITE_SPACE.matcher(line).matches())
			{
				// Skip any line while in skip mode.
				// Skip empty lines.
			}
			else if (null==type)
			{	
				if (null != classdef && !"sequence".equals(line) && !line.startsWith("structure") && (classdef.getEnvParam(line) == null))
				{
					skip = true;
					type=line;
				}
				else
					type=line.intern();
			}
			else
			{
				seq.append(line);
			}
		}
		Protein[] prots = proteins.values().toArray(new Protein[proteins.size()]);
//		Arrays.sort(prots);
		int protnum=0;
		for(Protein prot : prots)
		{
			prot.sortStrucInfo();
//			if (prot.hasStrucInfo())
//				System.err.println("#DEBUG: Protein "+prot.getName()+" has "+prot.getStrucInfoSize()+" structure info sequences.");
			prot.setNumber(protnum++);
		}
		return prots;
	}
}
