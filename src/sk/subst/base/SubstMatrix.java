/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
/**************************************************************************/
package sk.subst.base;

import sk.common.StringFunctions;
import sk.common.struc.DoubleMatrix;

public class SubstMatrix extends DoubleMatrix
{
	private static final long serialVersionUID = 1L;
	
	private char[] labels;
	
	public SubstMatrix(char[] labels)
	{
		super(labels.length);
		this.labels = labels;
	}
	
	// DEBUG printing...
	
//	public void increment(int x, int y, double value)
//	{
//		super.increment(x, y, value);
//		if (8==x && 8==y)
//			System.err.println("8,8 + "+value+" = "+getDouble(x,y));
//	}
//	
//	public void incrementAll(double value)
//	{
//		super.incrementAll(value);
//		System.err.println("8,8 + "+value+" = "+getDouble(8,8));
//	}
//	
//	public void multiplyAll(double factor)
//	{
//		super.multiplyAll(factor);
//		System.err.println("8,8 * "+factor+" = "+getDouble(8,8));
//	}
//	
//	public void set(int x, int y, double value)
//	{
//		super.set(x, y, value);
//		if (8==x && 8==y)
//			System.err.println("8,8 = "+value);
//	}
	
	
//	@Override
//	public String toFloatString()
//	{
//		final int size=labels.length;
//		final int decimals=2;
//		int[] colwidth = new int[size];
//		for(int i=0; i<size; i++)
//			colwidth[i] = StringFunctions.getColumnWidth(getColumnMaxAbs(i, false), 2);
//		
//		StringBuilder sb = new StringBuilder();
//		
//		// Column headers
//		
//		sb.append("  ");
//		for(int y=0; y<size-1; y++)
//		{
//			sb.append(String.format("%"+colwidth[y]+"s ", labels[y]));
//		}
//		sb.append(String.format("%"+colwidth[size-1]+"s\n", labels[size-1]));
//		sb.append('\n');
//		for(int x=0; x<size; x++)
//		{
//			sb.append(labels[x]);
//			sb.append(' ');
//			for(int y=0; y<size-1; y++)
//			{
//				sb.append(String.format("% "+colwidth[y]+"."+decimals+"f ", getDouble(x,y)));
//			}
//			sb.append(String.format("% "+colwidth[size-1]+"."+decimals+"f\n", getDouble(x, size-1)));
//		}
//		return sb.toString();
//	}
	
	@Override
//	public String toFloatStringReverse()
	public String toFloatString()
	{
		final int size=labels.length;
		final int decimals=2;
		int[] colwidth = new int[size];
		for(int i=0; i<size; i++)
			colwidth[i] = StringFunctions.getColumnWidth(getColumnMaxAbs(i), 2);
		
		StringBuilder sb = new StringBuilder();
		
		// Column headers
		
		sb.append("# ");
		for(int i=0; i<size-1; i++)
		{
			sb.append(String.format("%"+colwidth[i]+"s ", labels[i]));
		}
		sb.append(String.format("%"+colwidth[size-1]+"s\n", labels[size-1]));
//		sb.append('\n');
		for(int y=0; y<size; y++)
		{
			sb.append(labels[y]);
			sb.append(' ');
			for(int x=0; x<size-1; x++)
			{
				sb.append(String.format("% "+colwidth[x]+"."+decimals+"f ", getDouble(x,y)));
			}
			sb.append(String.format("% "+colwidth[size-1]+"."+decimals+"f\n", getDouble(size-1, y)));
		}
		return sb.toString();
	}
	

	@Override
//	public String toIntStringReverse()
	public String toIntString()
	{
		final int size=labels.length;
		int[] colwidth = new int[size];
		for(int i=0; i<size; i++)
			colwidth[i] = StringFunctions.getColumnWidth(round(getColumnMaxAbs(i)));
		
		StringBuilder sb = new StringBuilder();
		
		// Column headers
		
		sb.append("# ");
		for(int i=0; i<size-1; i++)
		{
			sb.append(String.format("%"+colwidth[i]+"s ", labels[i]));
		}
		sb.append(String.format("%"+colwidth[size-1]+"s\n", labels[size-1]));
//		sb.append('\n');
		for(int y=0; y<size; y++)
		{
			sb.append(labels[y]);
			sb.append(' ');
			for(int x=0; x<size-1; x++)
			{
				sb.append(String.format("% "+colwidth[x]+"d ", round(getDouble(x,y))));
			}
			sb.append(String.format("% "+colwidth[size-1]+"d\n", round(getDouble(size-1, y))));
		}
		return sb.toString();
	}
	
	
//	@Override
//	public String toIntString()
//	{
//		final int sizex=sizex(), sizey=sizey();
//		String[] formats = new String[sizey];
//		for(int i=0; i<sizey; i++)
//			formats[i] = StringFunctions.getColumnFormat(round(getColumnMaxAbs(i, false)));
//
//		StringBuilder sb = new StringBuilder();
//		for(int x=0; x<sizex; x++)
//		{
//			for(int y=0; y<sizey-1; y++)
//			{
//				sb.append(String.format(formats[y]+' ', round(getDouble(x,y))));
//			}
//			sb.append(String.format(formats[sizey-1]+'\n', round(getDouble(x, sizey-1))));
//		}
//		return sb.toString();
//	}
//	@Override
//	public String toIntStringReverse()
//	{
//		final int sizex=sizex(), sizey=sizey();
//		String[] formats = new String[sizex];
//		for(int i=0; i<sizex; i++)
//			formats[i] = StringFunctions.getColumnFormat(round(getColumnMaxAbs(i, true)));
//
//		StringBuilder sb = new StringBuilder();
//		for(int y=0; y<sizey; y++)
//		{
//			for(int x=0; x<sizex-1; x++)
//			{
//				sb.append(String.format(formats[x]+' ', round(getDouble(x,y))));
//			}
//			sb.append(String.format(formats[sizex-1]+'\n', round(getDouble(sizex-1, y))));
//		}
//		return sb.toString();
//	}
	
	
	public void makeSymmetric()
	{
		int size = sizex();
		if (size != sizey())
			throw new RuntimeException("cannot make matrix of asymmetric dimensions symmetric!");
		for (int i=0; i<size; i++)
		{
			for (int j=i+1; j<size; j++)
			{
				double value = (getDouble(i, j) + getDouble(j, i))/2;
				set(i, j, value);
				set(j, i, value);
			}
		}
	}
	
	
	public static void main(String[] args)
	{
		// Testing the toString functions
		//
		DoubleMatrix mat = new SubstMatrix(Env.getAAs());
		System.out.printf("x=%d, y=%d\n", mat.sizex(), mat.sizey());
		System.out.println();
		double value=0;
		for(int i=0; i<mat.sizex(); i++)
		{
			value = Math.floor(value);
			//value+=1;
			for(int j=0; j<mat.sizey(); j++)
			{
				value+=1;
				mat.set(i, j, value);
				//mat.set(i, j, (Math.random()>0.5?1:-1)*(Math.pow(Math.random()*100, 8*Math.random())));
			}
		}
		System.out.println(mat.toIntString());
		
		//DoubleMatrix mat2 = mat.clone();
		
		//mat2.multiplyAll(2);
		//System.out.println("==============================");
		//System.out.println(mat2.toIntString());
		System.out.println("==============================");
		System.out.println(mat.toFloatString());
		System.out.println("==============================");
	}
}
