package sk.subst.base;

public class EnvParam implements Comparable<EnvParam>
{
	public String name;
	public String values;
	public String labels;
	public boolean constrained;
	public boolean mask;
	
	public int compareTo(EnvParam o)
	{
		return name.compareTo(o.name);
	}
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof EnvParam))
			return false;
		return name.endsWith(((EnvParam)o).name);
	}
	@Override
	public int hashCode()
	{
		return name.hashCode();
	}
}
