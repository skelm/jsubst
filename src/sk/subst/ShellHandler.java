/**************************************************************************
 * 
 *  JSUBST is a program to calculate environment-specific substitution
 *  tables from annotated protein sequence alignments. Alignments may
 *  be between at least one protein of known structure and any number
 *  of protein sequences.
 *  
 *  Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)
 *  
 *  This file is part of JSUBST.
 *  
 *  JSUBST is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *  
 *  JSUBST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with JSUBST. If not, see <http://www.gnu.org/licenses/>.
 *  
 **************************************************************************/

/**************************************************************************
 * 
 * JSUBST makes use of Getopt.java -- Java port of GNU getopt from glibc 2.0.6
 *
 * Copyright (c) 1987-1997 Free Software Foundation, Inc.
 * Java Port Copyright (c) 1998 by Aaron M. Renn (arenn@urbanophile.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as published 
 * by  the Free Software Foundation; either version 2 of the License or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; see the file COPYING.LIB.  If not, write to 
 * the Free Software Foundation Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307 USA
 **************************************************************************/


package sk.subst;


import gnu.getopt.Getopt;

import java.io.*;
import java.util.*;

import sk.common.struc.*;
import sk.subst.base.*;


public class ShellHandler
{
	private static final String USAGE = "" +
			"JSUBST - A program to calculate environment-specific substitution tables\n" +
			"\n" +
			"Copyright 2008,2009,2010,2011  Sebastian Kelm (kelm@stats.ox.ac.uk)\n" +
			"Licenced under the GNU General Public License, version 3 (or later).\n" +
			"For details see the README and COPYING files, distributed with JSUBST.\n" +
			"\n" +
			"USAGE:\n" +
			"\tjsubst [OPTIONS] classdef_file tem_file...\n" +
			"\n" +
			"\tType 'jsubst -h' for a list of options.";
	
	private JSubst js;
	private boolean tabularOutput;
	private String titleSuffix = "";
	
	
	public ShellHandler(JSubst js)
	{
		this.js = js;
	}
	
	
	public JSubst getJSubst()
	{
		return js;
	}
	
	
	public boolean isTabulatOutput()
	{
		return tabularOutput;
	}
	public void setTabularOutput(boolean tabularOutput)
	{
		this.tabularOutput = tabularOutput;
	}
	
	public void setTitleSuffix(String suffix)
	{
		if (null==suffix)
			suffix = "";
		this.titleSuffix = suffix;
	}
	
	
	// ////////////////////////////////////////////////////////////////
	
	
	public void printEnvCounts()
	{
		int i=0;
		for (Env env : js.getEnvirs())
		{
			if (tabularOutput)
			{
				Env.printFlatMatrix(System.out, env.getCounts(), false, env.getLabel()+" COUNTS"+titleSuffix);
			}
			else
			{
				Env.printMatrix(System.out, env.getCounts(), false, env.getLabel()+" "+(i++)+" COUNTS"+titleSuffix);
			}
		}
	}

	public void printEnvProbabs()
	{
		int i=0;
		for (Env env : js.getEnvirs())
		{
			NumMatrix mat = env.getProbab().clone();
			mat.multiplyAll(100);
			if (tabularOutput)
			{
				Env.printFlatMatrix(System.out, mat, false, env.getLabel()+" PROBABILITIES"+titleSuffix);
			}
			else
			{
				Env.printMatrix(System.out, mat, false, env.getLabel()+" "+(i++)+" PROBABILITIES"+titleSuffix);
			}
		}
			
	}

	public void printEnvLogOdds()
	{
		int i=0;
		for (Env env : js.getEnvirs())
		{
			if (tabularOutput)
			{
				Env.printFlatMatrix(System.out, env.getLogOdd(), false, env.getLabel()+" LOG_ODDS"+titleSuffix);
			}
			else
			{
				Env.printMatrix(System.out, env.getLogOdd(), true, env.getLabel()+" "+(i++)+" LOG_ODDS"+titleSuffix);
			}
		}
	}

	public void printTotalCounts()
	{
		if (tabularOutput)
		{
			Env.printFlatMatrix(System.out, js.getGlobalEnvir().getCounts(), false, "TOTAL COUNTS"+titleSuffix);
		}
		else
		{
			Env.printMatrix(System.out, js.getGlobalEnvir().getCounts(), true, "TOTAL "+(js.getEnvirs().length)+" COUNTS"+titleSuffix);
		}
	}

	public void printTotalProbabs()
	{
		NumMatrix mat = js.getGlobalEnvir().getProbab().clone();
		mat.multiplyAll(100);
		if (tabularOutput)
		{
			Env.printFlatMatrix(System.out, mat, false, "TOTAL PROBABILITIES"+titleSuffix);
		}
		else
		{
			Env.printMatrix(System.out, mat, false, "TOTAL PROBABILITIES"+titleSuffix);
		}
	}

	public void printTotalLogOdds()
	{
		if (tabularOutput)
		{
			Env.printFlatMatrix(System.out, js.getGlobalEnvir().getLogOdd(), false, "TOTAL LOG_ODDS"+titleSuffix);
		}
		else
		{
			Env.printMatrix(System.out, js.getGlobalEnvir().getLogOdd(), true, "TOTAL LOG_ODDS"+titleSuffix);
		}
	}
	
	
	// ////////////////////////////////////////////////////////////////
	
	
	public void printCountSum()
	{
		// Print some debug information
		double sum=0;
		for(Env env : js.getEnvirs())
		{
			double count = env.getCounts().getSum();
			sum += count;
			System.err.printf(titleSuffix+"Env %s total counts: %f\n", env.getLabel(), count);
		}
		System.err.printf(titleSuffix+"Overall total counts: %f\n", sum);
	}
	
	
	// ////////////////////////////////////////////////////////////////

	
	@SuppressWarnings("unused")
	private static final boolean isTrue(String arg)
	{
		return null != arg && (arg.startsWith("T") || arg.startsWith("t"));
	}
	
	private static final boolean isFalse(String arg)
	{
		return null != arg && (arg.startsWith("F") || arg.startsWith("f"));
	}
	
	
	public static JSubst initialiseJSubst(Map<Character, Double> options, String classdefFile, String... temFiles)
	{
		Set<Character> optBool = options.keySet();

		// Initialise the environment class definitions
		//
		ClassDef classdef = new ClassDef(classdefFile);
		
		
		// Initialise the JSubst object and feed it the class definitions
		//
		JSubst jsubst = new JSubst();
		jsubst.setDebug(optBool.contains('d'));
		jsubst.setEnvirs(classdef.envirs);
		
		
		// Apply more options
		//
		jsubst.setClustCutoff(options.get('w')); // sequence identity cut-off for clustering
		jsubst.setMinPID(options.get('x')); // min sequence identity
		jsubst.setMaxPID(options.get('y')); // max sequence identity
		jsubst.setRawCount(optBool.contains('r')); // disable clustering ?
		jsubst.setCountingMode(JSubst.MODE.values()[options.get('m').intValue()]); // get counting mode (command line option 'm')
		
		if (optBool.contains('v'))
		  jsubst.setClusteringMode(BinaryCluster.MODE.AVERAGE);
		
		
		// Add supplied amount to all counts
		//
		if (options.get('a') != 0.0)
			jsubst.addToCounts(options.get('a'));
		
		
		// Sum substitution counts for all .tem files provided in the
		// arguments
		//
		for (int i = 0; i < temFiles.length; i++)
		{
			String filename = temFiles[i];

			// Parse the current .tem file.
			// Feed the proteins from the .tem file into JSubst.
			//
			try
			{
				Protein[] proteins = Tem.parseTemFile(filename, classdef);
				jsubst.setProts(proteins);
			}
			catch (IOException e)
			{
				if (e instanceof FileNotFoundException)
				{
					// The specified .tem file does not exist.
					// Print an error and skip to the next one.
					//
					System.err.println("ERROR: File not found. Skipping : " + filename);
					continue;
				}
				else
				{
					throw new RuntimeException(e);
				}
			}
			
			// Count substitutions between the proteins in the current .tem file
			//
			int subs = jsubst.calculateCounts();
			System.err.printf("%20s: %7d (raw) substitutions counted\n", filename, subs);
		}
		
		
		// If we have found no usable .tem file, abort with an error message
		//
		if (null == jsubst.getProts())
		{
			System.err.println("No proteins to count substitutions in. No usable .tem files found within arguments: "+Arrays.toString(temFiles));
			System.exit(2);
		}
		
		return jsubst;
	}
	
	
	
	// ////////////////////////////////////////////////////////////////
	
	
	public static void main(String[] args)
	{
		// Data structures holding options and their arguments.
		//
		Map<Character, Double> options = new HashMap<Character, Double>(); // all option keys (boolean or otherwise) + numeric arguments
		Set<Character> optBool = options.keySet(); // for checking whether an option was provided
		Map<Character, String> optString = new HashMap<Character, String>(); // String arguments
		
		// Index of the first non-option argument
		//
		int firstNonOptArg;
		
		
		// Initialise default values for command line options
		//
		options.put('a', 1.0);   // add 1.0 to all counts by default        -- DELETING THIS LINE WILL RESULT IN AN EXCEPTION BEING THROWN!
		options.put('w', 60.0);  // set clustering cutoff to 60% protein ID -- DELETING THIS LINE WILL RESULT IN AN EXCEPTION BEING THROWN!
		options.put('x', 0.0);   // set min sequence identity to 0%         -- DELETING THIS LINE WILL RESULT IN AN EXCEPTION BEING THROWN!
		options.put('y', 100.0); // set max sequence identity to 100%       -- DELETING THIS LINE WILL RESULT IN AN EXCEPTION BEING THROWN!
		//options.put('l', null); // print Log-Odds
		//options.put('t', null); // print Total matrices
		options.put('m', 2.0);  // set substitution counting mode to 2 : structure-cluster to all
		
		
		
		/*
		 * Command line option parsing
		 */
		
		{
			Getopt g = new Getopt("jsubst", args, "h3jgdcpletbrm:vw:a:o:s:x:y:zi"); // GNU getopt command line option definition
			
			String arg; // will hold the String argument of any non-boolean option  (returned by getopt)
			int i; // will hold the int representation of the current option character (returned by getopt)
			char c; // will hold the current option character  (returned by getopt)
			
			// Parse command line options
			//
			while ((i = g.getopt()) >= 0)
			{
				c = (char)i;
				arg = g.getOptarg();
				switch (c)
				{
					case 'h':
						System.err.println(USAGE);
						System.err.println();
						System.err.println("EXAMPLE COMMAND:");
						System.err.println("\t");
						System.err.println("\tjsubst -etl -a 1.0 -w 60.0 -m 2 -o matrices.txt -s messages.txt classdef.dat *.tem");
						System.err.println("\t");
						System.err.println("\tThis will output environment-specific tables (-e), and a total table (-t), all in\n" +
										   "\tlog-odds format (-l). Exactly 1.0 is added to the total counts (-a), meaning that\n" +
										   "\t1.0/(number of environments) is added to each environment table. Clustering is at\n" +
										   "\ta level of 60% sequence identity (-w). The counting mode is 2 (-m), meaning that\n" +
										   "\tsubstitutions are counted from each cluster containing proteins of known structure\n" +
										   "\tto all other clusters. Output is saved in 'matrices.txt' (-o), status messages are\n" +
										   "\tsaved in 'messages.txt' (-s). The file 'classdef.dat' should contain SUBST or\n" +
										   "\tULLA-style environment definitions. The shell pattern '*.tem' refers to a number of\n" +
										   "\tannotated alignments in the TEM file format, used by programmes such as JOY and\n" +
										   "\tiMembrane.");
						System.err.println("\t");
						System.err.println("OPTIONS:");
						System.err.println("\t");
						System.err.println("\t-h\tPrint this help message and exit");
						System.err.println("\t");
//						System.err.println("\t-i\tFirst non-option argument is a serialised java object");
//						System.err.println("\t  \t(When this option is set, a single non-option argument must be provided.)");
//						System.err.println("\t");
						System.err.println("\t-d\tPrint lots of debug messages");
						System.err.println("\t");
						System.err.println("\t-o FILE\tRedirect standard output to the named FILE");
						System.err.println("\t-s FILE\tRedirect status/error messages to the named FILE");
						System.err.println("\t");
						System.err.println("\t-j\tAdd half-cystein (J) to the list of valid amino acids");
						System.err.println("\t-g\tAdd the gap character (-) to the list of valid amino acids");
						System.err.println("\t");
						System.err.println("\t-3\tPrint flattened 3D matrices, with one line per input tem file\n" +
										   "\t  \t(This option is not compatible with -i and -o.");
						System.err.println("\t");
						System.err.println("\t-c\tPrint count tables");
						System.err.println("\t-p\tPrint probability tables");
						System.err.println("\t-l\tPrint log-odds tables");
						System.err.println("\t");
						System.err.println("\t-e\tPrint tables for all single environments");
						System.err.println("\t-t\tPrint total (summed count) tables");
						System.err.println("\t");
						System.err.println("\t-b\tPrint background substitution probabilities");
						System.err.println("\t");
//						System.err.println("\t-s\tCount sequence-sequence substitutions (All vs All) (default: F)");
						System.err.println("\t-m NUM\tSubstitution counting mode (default: 2):");
						System.err.println("\t     \t 0 : Structure to structure, constrained environment");
						System.err.println("\t     \t 1 : Structure to structure");
						System.err.println("\t     \t 2 : Structure-cluster to all");
						System.err.println("\t     \t 3 : All to all");
						System.err.println("\t");
						System.err.println("\t-r\tCount 'raw' substitutions only, and disable clustering");
						System.err.println("\t-v NUM\tUse average linkage clustering, rather than the default single linkage.");
						System.err.println("\t-w NUM\tSet clustering (PID) level for BLOSUM-like weighting to NUM (default: 60.0)");
						System.err.println("\t     \t(NUM is a floating point number from 0 to 100)");
						System.err.println("\t-x NUM\tSet minimum sequence identity (PID) level to NUM (default: 0.0)");
						System.err.println("\t-y NUM\tSet maximum sequence identity (PID) level to NUM (default: 100.0)");
						System.err.println("\t");
						System.err.println("\t-a NUM\tAdd NUM to all total counts (default: 1.0)");
						System.err.println("\t");
						System.err.println("\t-z\tForce count matrices to be symmetric by averaging counts for A(a->b) and A(b->a).");
						System.err.println("\t");
						System.err.println("\t-i\tPrint composition counts for each environment.");
//						System.err.println("\t");
//						System.err.println("\t-oFILE\tOutput serialised java object to FILE");
//						System.err.println("\t");
						System.exit(1);
					case 'd':
					case 'c':
					case 'p':
					case 'l':
					case 'e':
					case 't':
					case 'b':
					case 'r':
					case 'v':
//					case 's':
//					case 'i':
					case 'j':
					case 'g':
					case '3':
					case 'z':
					case 'i':
						if (!isFalse(arg))
							options.put(c, null);
						else
							options.remove(c);
						break;
					case 'w':
					case 'x':
					case 'y':
					case 'a':
					case 'm':
						options.put(c, Double.parseDouble(arg));
						break;
					case 'o':
					case 's':
						options.put(c, null);
						optString.put(c, arg);
						break;
					case '?':
						System.err.println(USAGE);
						System.exit(2); // getopt() already printed an error
					default:
						System.err.println("Unhandled command line argument: "+c);
						System.err.println("This is most likely a bug. Check for a more recent version of this programme.");
						System.err.println(USAGE);
						System.exit(2);
				}
			}
			
			firstNonOptArg = g.getOptind();
			
			
//			if (optBool.contains('i'))
//			{
//				int nonopts = args.length - firstNonOptArg;
//				if (nonopts != 1)
//				{
//					System.err.println("A single non-option argument must be provided, when option 'i' is enabled. Provided: "+nonopts);
//					System.err.println(USAGE);
//					System.exit(2);
//				}
//			}
//			else if (args.length - firstNonOptArg < 2)
			if (args.length - firstNonOptArg < 2)
			{
				// If we have less than two non-option arguments, exit with a usage help message.
				System.err.println(USAGE);
				System.exit(2);
			}
		}
		
		
//	    if (optBool.contains('3') && optBool.contains('o'))
//	    {
//	      System.err.println("ERROR: Options -o and -3 are mutually exclusive.");
//	      System.err.println(USAGE);
//	      System.exit(2);
//	    }
//	    if (optBool.contains('3') && optBool.contains('i'))
//	    {
//	      System.err.println("ERROR: Options -i and -3 are mutually exclusive.");
//	      System.err.println(USAGE);
//	      System.exit(2);
//	    }
		
	    
		
		if (optBool.contains('j'))
		{
			// Consider half-cysteine (J) as an amino acid?
			//
			Env.setValidAminoAcids(Env.getValidAminoAcids()+"J");
		}	
		if (optBool.contains('g'))
		{
			// Consider a gap (-) as an amino acid?
			//
			Env.setValidAminoAcids(Env.getValidAminoAcids()+"-");
		}
		
		
		/*
		 * End of command line option initialisation
		 */
		
		
		/*
		 * Output redirection
		 */
		
		if (optBool.contains('o'))
		{
			try
			{
				if (optBool.contains('d'))
					System.err.println("Redirecting output to file: " + optString.get('o'));
				System.setOut(
					    new PrintStream(
					       new FileOutputStream(optString.get('o'))));
			}
			catch (FileNotFoundException e)
			{
				throw new RuntimeException(e);
			}
		}
		
		if (optBool.contains('s'))
		{
			try
			{
				if (optBool.contains('d'))
					System.err.println("Redirecting errors to file: " + optString.get('s'));
				System.setErr(
					    new PrintStream(
					       new FileOutputStream(optString.get('s'))));
			}
			catch (FileNotFoundException e)
			{
				throw new RuntimeException(e);
			}
		}
		
		
		/*
		 * The main part of the programme starts here ...
		 */
	
	
		String classdef = null;
		
//		if (!optBool.contains('i'))
//		{
//			classdef = args[firstNonOptArg];
//			args = Arrays.asList(args).subList(firstNonOptArg+1, args.length).toArray(new String[0]);
//		}
//		else
//		{
//			args = Arrays.asList(args).subList(firstNonOptArg, args.length).toArray(new String[0]);
//		}
		classdef = args[firstNonOptArg];
		args = Arrays.asList(args).subList(firstNonOptArg+1, args.length).toArray(new String[0]);
		
		
		// Any merging of environments should go here
		
		
		if (optBool.contains('3'))
		{
			System.out.print("!ENVNAME MATRIXTYPE FILENAME\t");
			Env.printFlatMatrixLabels(System.out);
		}
		
		
		int args_index = 0;
		while ((args_index < args.length) && (optBool.contains('3') || args_index < 1))
		{
		  
			ShellHandler shell;
			JSubst jsubst;
		
		  	
			////////////////////////// BEGIN Load Input //////////////////////////
	
//			if (optBool.contains('i'))
//			{
//				// must not have received -3 option
//	
//				jsubst = (JSubst)Serialization.deserializeFromFile(args[0]);
//				if (null==jsubst)
//				{
//					System.err.println("ERROR reading Java object from file: "+args[0]);
//					System.err.println(USAGE);
//					System.exit(2);
//				}
//				jsubst.setDebug(optBool.contains('d'));
//				shell = new ShellHandler(jsubst);
//			}
//			else
//			{
				if (optBool.contains('3'))
				{
					String s = args[args_index];
					jsubst = initialiseJSubst(options, classdef, s);
					shell = new ShellHandler(jsubst);
					shell.setTabularOutput(true);
					shell.setTitleSuffix(" "+s);
				}
				else
				{
					jsubst = initialiseJSubst(options, classdef, args);
					shell = new ShellHandler(jsubst);
				}
//			}
			++args_index; // increment argument index
	
			////////////////////////// END   Load Input //////////////////////////
	  
			
			// Force count matrices to be symmetric
			//
			if (optBool.contains('z'))
			{
				jsubst.makeSymmetric();
			}
			
			
			// Calculate observed probabilities for amino acid substitutions
			//
			if (optBool.contains('p'))
			{
				jsubst.calculateProbab();
			}
			
			// Calculate log-odds scores for observed amino acid substitutions
			//
			if (optBool.contains('l'))
			{
				jsubst.calculateLogOdds();
			}
			
			
			shell.printCountSum();
			
			
			// Print environment-dependent tables
			//
			if (optBool.contains('e'))
			{
				if (optBool.contains('c'))
				{
					shell.printEnvCounts();
				}
				if (optBool.contains('p'))
				{
					shell.printEnvProbabs();
				}
				if (optBool.contains('l'))
				{
					shell.printEnvLogOdds();
				}
			}
			
			// Print environment-independent tables
			//
			if (optBool.contains('t'))
			{
				if (optBool.contains('c'))
				{
					shell.printTotalCounts();
				}
				if (optBool.contains('p'))
				{
			//					jsubst.getGlobalEnvir().getProbab().multiplyAll(100); // TODO: make this a non-permanent operation
					shell.printTotalProbabs();
				}
				if (optBool.contains('l'))
				{
					shell.printTotalLogOdds();
				}
			}
			
			// Print background probabilities of amino acids replacing others
			//
			if (optBool.contains('b'))
			{
				System.out.println("\n\n\n\n");
				JSubst.AaProb[] bgProbabs = jsubst.getSortedBgProbabs();
				System.out.println("# Background probabilities of any amino acid being substituted by...");
				for (JSubst.AaProb bgp : bgProbabs)
				{
					System.out.println("#     " + bgp.aa + " : " + bgp.prob);
				}
			}
			
			
			// Print composition counts
			//
			if (optBool.contains('i'))
			{
				System.out.print("#         ");
				System.out.print(jsubst.getGlobalEnvir().getCompositionCounts().getHeaderString());
				System.out.print(String.format("%-9s ", jsubst.getGlobalEnvir().getLabel()));
				System.out.print(jsubst.getGlobalEnvir().getCompositionCounts().toFloatString());
				for (Env env : jsubst.getEnvirs())
				{
					System.out.print(String.format("%-9s ", env.getLabel()));
					System.out.print(env.getCompositionCounts().toFloatString());
				}
			}
			
			
//			// Serialize JSubst instance, if corresponding option given
//			//
//			if (optBool.contains('o'))
//			{
//			// must not have received -3 option
//			
//				try
//				{
//					Serialization.serializeToFile(jsubst, optString.get('o'));
//					System.err.println("Java object serialised to file: "+optString.get('o'));
//				}
//				catch (IOException e)
//				{
//					throw new RuntimeException(e);
//				}
//			}
		}
	}
}
